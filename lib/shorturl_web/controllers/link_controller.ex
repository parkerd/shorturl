defmodule ShorturlWeb.LinkController do
  use ShorturlWeb, :controller

  alias Shorturl.Links
  alias Shorturl.Links.Link

  def index(conn, _params) do
    changeset = Links.change_link(%Link{})
    render(conn, "index.html", changeset: changeset)
  end

  def create(conn, %{"link" => link_params}) do
    case Links.create_link(link_params) do
      {:ok, link} ->
        conn
        |> redirect(to: Routes.link_path(conn, :show, link))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "index.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    link = Links.get_link!(id)
    url = URI.to_string(URI.merge(URI.parse(ShorturlWeb.Endpoint.url()), "/#{link.id}"))
    render(conn, "show.html", url: url)
  end

  def redirect_url(conn, %{"id" => id}) do
    link = Links.get_link!(id)

    conn
    |> redirect(external: link.url)
  end
end
