defmodule Shorturl.Links.LinkId do
  use Ecto.Type

  def type, do: :string

  def cast(id) when is_binary(id) do
    {:ok, id}
  end

  def cast(_), do: :error

  def dump(id) when is_binary(id) do
    {:ok, id}
  end

  def dump(_), do: :error

  def load(id) when is_binary(id) do
    {:ok, id}
  end

  def load(_), do: :error

  def autogenerate do
    :crypto.strong_rand_bytes(3) |> Base.url_encode64()
  end
end
