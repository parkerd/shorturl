defmodule Shorturl.Links.Link do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, Shorturl.Links.LinkId, autogenerate: true}
  schema "links" do
    field :url, :string

    timestamps()
  end

  @doc false
  def changeset(link, attrs) do
    link
    |> cast(attrs, [:url])
    |> validate_required([:url])
    |> validate_url(:url)
  end

  def validate_url(changeset, field, opts \\ []) do
    validate_change(changeset, field, fn _, value ->
      uri = URI.parse(value)

      case uri.scheme == nil do
        true -> [{field, opts[:message] || "Invalid URL, ensure it starts with http or https"}]
        false -> []
      end
    end)
  end
end
