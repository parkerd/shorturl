# ShortURL

## Requirements

- Elixir v1.11.3
- NodeJS v14 (`lts/fermium`)
  - Configs are included for `direnv` and `nvm`
  - Does not work with v15, see https://github.com/phoenixframework/phoenix/issues/4126
- `docker` and `docker-compose` (included in Docker Desktop) or standalone Postgres

## Running

1) `make setup` to install dependencies
2) `make db` to start postgres with `docker-compose`
    - First setup may take 5-10 seconds
3) `make server` and open http://localhost:4000
4) `make test`
5) `make down` to stop the database
