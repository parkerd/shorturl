defmodule ShorturlWeb.LinkControllerTest do
  use ShorturlWeb.ConnCase

  alias Shorturl.Links

  @create_attrs %{url: "http://example.com"}
  @empty_attrs %{url: nil}
  @invalid_attrs %{url: "test"}

  def fixture(:link) do
    {:ok, link} = Links.create_link(@create_attrs)
    link
  end

  describe "index" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.link_path(conn, :index))
      assert html_response(conn, 200) =~ "New URL"
    end
  end

  describe "create link" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.link_path(conn, :create), link: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.link_path(conn, :show, id)

      conn = get(conn, Routes.link_path(conn, :show, id))
      assert html_response(conn, 200) =~ "URL Created"
    end

    test "renders errors when data is empty", %{conn: conn} do
      conn = post(conn, Routes.link_path(conn, :create), link: @empty_attrs)
      assert html_response(conn, 200) =~ "New URL"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.link_path(conn, :create), link: @invalid_attrs)
      assert html_response(conn, 200) =~ "New URL"
    end
  end

  describe "redirect to link" do
    setup [:create_link]

    test "redirects to link url", %{conn: conn, link: link} do
      conn = get(conn, Routes.link_path(conn, :redirect_url, link))
      assert redirected_to(conn) == link.url
    end
  end

  defp create_link(_) do
    link = fixture(:link)
    %{link: link}
  end
end
