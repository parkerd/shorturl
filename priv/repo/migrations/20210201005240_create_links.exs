defmodule Shorturl.Repo.Migrations.CreateLinks do
  use Ecto.Migration

  def change do
    create table(:links, primary_key: false) do
      add :id, :string, primary_key: true
      add :url, :string

      timestamps()
    end

  end
end
